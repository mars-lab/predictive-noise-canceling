# Lowering The Acoustic Noise Burden in MRI with Predictive Noise Canceling

This repository contains experimental acoustic noise data used in predictive noise canceling (PNC) experiments, as well as supplementary videos for animated abstract and audio-visual noise reduction illustrations.

## Supplementary videos

[**S1 Animated abstract**](https://gitlab.tudelft.nl/mars-lab/predictive-noise-canceling/-/blob/f461b828042e74f27b5c537ee2f87e807c881245/Supplementary%20videos/S01%20animated%20abstract.mp4)

[**S2 Sequence 1**](https://gitlab.tudelft.nl/mars-lab/predictive-noise-canceling/-/blob/f461b828042e74f27b5c537ee2f87e807c881245/Supplementary%20videos/S02%20Sequence%201.mp4)

[**S3 Sequence 2**](https://gitlab.tudelft.nl/mars-lab/predictive-noise-canceling/-/blob/336606714da5af65acbea15fcc6f62644e8e73fb/Supplementary%20videos/S03%20Sequence%202.mp4)

[**S4 Sequence 3**](https://gitlab.tudelft.nl/mars-lab/predictive-noise-canceling/-/blob/336606714da5af65acbea15fcc6f62644e8e73fb/Supplementary%20videos/S04%20Sequence%203.mp4)

[**S5 Sequence 4**](https://gitlab.tudelft.nl/mars-lab/predictive-noise-canceling/-/blob/336606714da5af65acbea15fcc6f62644e8e73fb/Supplementary%20videos/S05%20Sequence%204.mp4)

[**S6 Sequence 5**](https://gitlab.tudelft.nl/mars-lab/predictive-noise-canceling/-/blob/336606714da5af65acbea15fcc6f62644e8e73fb/Supplementary%20videos/S06%20Sequence%205.mp4)

[**S7 Sequence 6**](https://gitlab.tudelft.nl/mars-lab/predictive-noise-canceling/-/blob/336606714da5af65acbea15fcc6f62644e8e73fb/Supplementary%20videos/S07%20Sequence%206.mp4)

[**S8 Sequence 7**](https://gitlab.tudelft.nl/mars-lab/predictive-noise-canceling/-/blob/336606714da5af65acbea15fcc6f62644e8e73fb/Supplementary%20videos/S08%20Sequence%207.mp4)

[**S9 Sequence 8**](https://gitlab.tudelft.nl/mars-lab/predictive-noise-canceling/-/blob/336606714da5af65acbea15fcc6f62644e8e73fb/Supplementary%20videos/S09%20Sequence%208.mp4)

[**S10 Sequence 9**](https://gitlab.tudelft.nl/mars-lab/predictive-noise-canceling/-/blob/336606714da5af65acbea15fcc6f62644e8e73fb/Supplementary%20videos/S10%20Sequence%209.mp4)

[**S11 Sequence 10**](https://gitlab.tudelft.nl/mars-lab/predictive-noise-canceling/-/blob/336606714da5af65acbea15fcc6f62644e8e73fb/Supplementary%20videos/S11%20Sequence%2010.mp4)

## Experimental acoustic noise data

[**Calibration**](https://gitlab.tudelft.nl/mars-lab/predictive-noise-canceling/-/tree/main/Experimental%20acoustic%20noise%20data/Calibration)

The PNC calibration audio recordings were acquired in three steps, always using a base sequence of a scanner-trigger synchronized gradient (TSG) with repetition time TR = 3 s:

* **Step 1**, where the TSG is interleaved with a 2 s break with a single X/Y/Z gradient pulse played on the scanner;

* **Step 2**, where the TSG is interleaved with a 2 s break with the acoustic noise prediction output played through the pneumatic transducer;

* **Step 3**, similar to step one but also superimposing the predicted anti-noise, resulting in attenuated acoustic noise.

The data acquired in this section was used to generate the X/Y/Z gradient coil transfer functions for the following sequence experiments, as well as to generate the triangular pulse noise reduction cascade shown in the Supplementary Figure S2 of the manuscript.

The data was acquired in six separate scanning sessions.

[**Sequence data**](https://gitlab.tudelft.nl/mars-lab/predictive-noise-canceling/-/tree/main/Experimental%20acoustic%20noise%20data/Fast%20sequence%20data)

The audio recordings of sequences were acquired in a similar manner to the calibration recordings, defining step 1 as the scanner-only recording; step 2 as prediction-only recording; and step 3 as the simultaneous output of the scanner sequence and predicted anti-noise. In each scanning session, five live reduction (step 3) repetitions were acquired for each sequence.

To evaluate noise variability of the sequences, five repetitions of scanner-only sequence recording were obtained in a separate scanning session.

[**Error sources**](https://gitlab.tudelft.nl/mars-lab/predictive-noise-canceling/-/tree/main/Experimental%20acoustic%20noise%20data/Error%20sources)

In PNC, feed-forward signal corrections were derived for channel distortion, latency and recorder clock-mismatch. The noise recordings relating to the evaluation of these corrections are presented in this section.

[**Sequence parameter study**](https://gitlab.tudelft.nl/mars-lab/predictive-noise-canceling/-/tree/main/Experimental%20acoustic%20noise%20data/Sequence%20parameter%20study)

To estimate the effect of acoustically-relevant MRI sequence parameters, imaging slice thickness, repetition time, receiver bandwidth and angulation were varied. The audio recordings were acquired in a similar manner to other sequence data, defining step 1 as scanner-only, step 2 as prediction-only, and step 3 as the superposition of scanner noise and predicted anti-noise (using five repetitions for the latter).

[**Time scale study**](https://gitlab.tudelft.nl/mars-lab/predictive-noise-canceling/-/tree/main/Experimental%20acoustic%20noise%20data/Time%20scale%20study)

In this section, audio recordings presented relate to the long sequence (2 minute) experiments: repeated single-TR; regular 2D sequence; regular 3D sequence. The recording are acquired in 3 step manner similar to other sequence experiments.

[**Linearity and LTI study data**](https://gitlab.tudelft.nl/mars-lab/predictive-noise-canceling/-/tree/main/Experimental%20acoustic%20noise%20data/Linearity%20and%20LTI%20study%20data)

To study the applicability of the Linear Time Invariant (LTI) model, the calibration sequence (step 1) was acquired with various pulse amplitudes and rise times.

## Licence

![CC BY ND](https://mirrors.creativecommons.org/presskit/buttons/88x31/png/by-nd.png)

## Contacts

Paulina Šiurytė (p.siuryte@tudelft.nl)

Sebatian Weingärtner (s.weingartner@tudelft.nl)

Technical University of Delft, Faculty of Applied Sciences

